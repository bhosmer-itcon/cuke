# OCIO Rancher Pipelines

## Introduction
OCIO wants to eliminate manual and labor-intensive steps and also deliver
software in a repeatable, rapid, secure, and audit-able way.

Continuous Integration and Delivery with pipelines helps achieve this.

## Rancher Configuration
Your Rancher administrator will need to configure Rancher with access to
a docker registry and version control credentials.

This needs to be done once for each project that you want to use pipelines with.

*Give this part to your Rancher administrator.*

1. From within the project that you want to use pipelines with, browse to Resource -> Pipelines.
2. Click "Configure Repositories"
3. At the bottom of the screen "Authorize and Fetch Your Repositories"
4. For EWAPS, choose "Bitbucket"
5. For EWAPS, choose "Bitbucket Cloud"
6. Rancher provides instructions to proceed
7. Choose your profile image in the left, then the organization, then settings.
8. Under "Apps and features", "Oauth consumers"
9. Create a consumer and retrieve the key and secret and add them to Rancher

## Assumptions
- Docroot is in `docroot/` and you have drush installed and in your repository
- Your `settings.php` uses environment variables for a more stateless application:

**NOTE**
In the examples files you'll see the name _cuke_. You should replace this with the name
of your application. Use something short but easily identifiable.

```
...
$dbhost = getenv('DBHOST');
$dbname = getenv('DBNAME');
$dbpass = getenv('DBPASS');
$dbuser = getenv('DBUSER');
$dbport = getenv('DBPORT');

$databases['default']['default'] = array(
  'database' => $dbname,
  'username' => $dbuser,
  'password' => $dbpass,
  'host' => $dbhost,
  'driver' => 'mysql',
  'port' => $dbport,
  'prefix' => '',
...
```
- Your rancher cluster is already configured with bitbucket and registry credentials.

## Pre-requisites
Your project needs a `Secret` and `configMap`. These contain the sensitive values for your database connection and the other booleans that control drush operations on deployment.

See Rancher's specific [documentation](https://rancher.com/docs/rancher/v2.x/en/k8s-in-rancher/configmaps/) as well as the Kubernetes [documentation](https://kubernetes.io/docs/concepts/configuration/configmap/).

Rancher's documentation on [secrets](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/) and the Kubernetes [documentation](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/).

These are then provided as environment variables when your pods start.

Your secrets should contain the following keys with values:

```
DBHOST
DBNAME
DBPASS
DBPORT
DBUSER
DOCROOT
```

Your config map:

```
APPNAME
DRUSHCIM (true or false)
DRUSHCR (true or false)
DRUSHUPDB (true or false)
```

A sample `configMap` is provided here. You'll need to specify your individual database
credentials etc. though.

As you work, update the `your-app.yaml` and edit the `ConfigMap` section to indicate
whether you'd like to execute the drush commands. Commit this file and Kubernetes will 
take care of it on your next update.

## The .rancher-pipeline.yaml
Rancher's [documentation](https://rancher.com/docs/rancher/v2.x/en/k8s-in-rancher/pipelines/example/) provides information regarding this file and the options available to it.

This is the file that controls your build and it must be in the root of your repository or
the pipelines won't work.

## The Dockerfile
Within your `.rancher-pipeline.yaml` you can specify the location of your Dockerfile.
One is provided in the repo. You'll need to copy that, along with the other configuration files at the root of this repository.

These include:

- `app-nginx.conf`
- `nginx.conf`
- `php.ini`
- `php-fpm.conf`
- `Dockerfile.builddeploy`
- `deploy-drupal.sh`

These are needed to build the image that your project will run.

## The build steps
1. Commit/push or other triggers
2. Rancher builds an image and copies your code into it
3. Rancher pushes that image to a registry (they can't tell me why since an internal registry exists during pipelines but whatever)
4. Rancher then deploys the following resources in your kube cluster
    - A deployment pod with and init container that handles drush housekeeping tasks
    - A webserver with your site's code
    - A service
    - An ingress
    - A CronJob
    - A ConfigMap (update)

These are documented and deployed from the `cuke-app.yaml`.

## Application Architecture

Since the `configMap` abstracts and stores the sensitive credentials you can make them
available to the containers running within your pod.

The pipeline automatically creates three different types of Kubernetes controllers:

1. A [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
  - This creates the webserver pod(s) that serves your application
  - These can be scaled up and down depending on needs
  - It is based on NGINX and uses php-fpm
2. A [CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)
  - This handles drupal cron tasks and can be changed depending on how frequently you want it to run
3. A [Job](https://kubernetes.io/docs/concepts/workloads/controllers/jobs-run-to-completion/)
  - This runs once to completion. It handles `drush {cim/updb/cr}`

