#!/bin/bash

pushd ${DOCROOT} >/dev/null 2>&1

PHP="/opt/rh/rh-php72/root/usr/bin/php"

if [ "$DRUSHCIM" = true ]; then
  $PHP /app/vendor/bin/drush cim -y --root /app/docroot --no-interaction
fi

if [ "$DRUSHCR" = true ]; then
  $PHP /app/vendor/bin/drush cr -y --root /app/docroot --no-interaction
fi

if [ "$DRUSHUPD" = true ]; then
  $PHP /app/vendor/bin/drush updb -y --root /app/docroot --no-interaction
fi

popd >/dev/null 2>&1

exit $?
